################################################################
## Octave script for solution of the eigenvalue problem
## using finite order approximation in Legendre polynomials
##
## TODO: Match the periodic boundary conditions on mu?
##       Isn't the function odd? Isn't the problem solved 
## 	 already for x > 0?
##
################################################################

s_u =  500;

global 		     N = 12;      ## Number of polynomials in the expansion. Needs to be even
global 		     E = 1000;    ## Number of evaluation steps t in the ploting routine
global 	            nu = 1;      ## Collision frequency parameter. Note the solution uses -nu from the original equation
global maxwellian_step = 1.0;     ## Maxwellian step across the shock

global 	       v_1 = 1.0;         ## Dummy vars again
global 	       v_2 = 1.0;	  ## 
global		 F = 250;	  ## Number of evaluation steps x in the shock-front reconstruction 	       

setenv GNUTERM 'wxt' 	          ## Use WXT for plotting

################################################################
## FUNCTION DEFINITIONS:
################################################################



## ---> FUNCTION: v_legendre_vector(s_x)
## Returns a row vector of Legendre polynomials

function v_leg = v_legendre_vector(s_x)
	global N;
	v_leg = zeros(N, 1);
	
	for n = 1:N
	    v_leg(n) = legendre_Pl(n-1,s_x);
	endfor
endfunction



## ---> FUNCTION: s_U(s_x, v_r_coefficients)
## Returns approximation to the exact solution to N-th order of Legendre expansion
## Takes a row vector of coefficients 

function s_y = s_U(s_x, v_coefficients)
       	s_y = v_coefficients*v_legendre_vector(s_x);
endfunction



################################################################
##  SOLUTION TO EIGENSYSTEM
################################################################


## ---> U operator - inverted U matrix
v_N_upper_diagonal = zeros(N-1, 1);
v_N_lower_diagonal = zeros(N-1, 1);

for n = 1:(N-1)
    v_N_upper_diagonal (n) = (n / (2*n +1));
    v_N_lower_diagonal (n) = ((n-1) / (2*n - 1));
endfor

## ---> diagonal elements are not given by a simple sequence here - 0 is skipped on lower diagonal.
v_N_lower_diagonal(1) = 1;	

m_NxN_U  = diag(v_N_upper_diagonal',1) + diag(v_N_lower_diagonal',-1)
m_NxN_IU = inv(m_NxN_U)

## ---> -nu - L/2 operator
v_N_diagonal = zeros(N,1);

for n = 0:(N-1)
    v_N_diagonal(n+1) = n*(n+1)/2;
endfor

m_NxN_L  = diag(v_N_diagonal) 
m_NxN_nu = nu*eye(N);
m_NxN_OP = m_NxN_L + m_NxN_nu;

## ---> Multiply LHS
m_NxN_LHS = m_NxN_IU*m_NxN_OP;

## ---> Solve the eigensystem
[m_NxN_eigenvectors, m_NxN_eigenvalues] = eig(m_NxN_LHS);

global v_Nr_eigenvalues = diag(m_NxN_eigenvalues)
v_Nr_eigenvalues

#################################################################
## PLOTTING
#################################################################
global Y = cell(N,1); 		         	## Array of vectors
t = linspace(-1.0,1.0, E); 			## Specify linear space 

for n = 1:N
    s_eigenvalue = v_Nr_eigenvalues(n);

    v_Nc_eigenvector = m_NxN_eigenvectors(:,[n]);
    v_Er_y  = zeros(E,1);

    for j = 1:E
	v_Er_y(j) = s_U(t(j), v_Nc_eigenvector');
    endfor

    Y(n) = v_Er_y;
endfor

shall_plot = input("Shall I plot the first six eigenfunctions? (Y/N) ", "s");

if shall_plot == "Y"
    plot(t, Y{1}, "1", t, Y{2}, "2", t, Y{3}, "3", t, Y{4}, "4", t, Y{5}, "5", t, Y{6}, "6")
endif

###################################################################
## CONTINUE WITH EVALUATION?
###################################################################

input("Continue matching the boundary conditions? (Y/N) ", "s");

if ans != "Y"
   quit 
endif



###################################################################
## USE THE DISCRETIZED FUNCTIONS TO MATCH THE BOUNDARY CONDITIONS
###################################################################

# NEGATIVE - refers to negative eigenvalues
# POSITIVE - refers to positive eigenvalues

global v_N_positive_coefs   = zeros(N/2,1); 
global v_N_positive_indices = ones(N/2,1);  ## this is a lame way of doing so, but.. :D
       s_n_positive_coefs = 1;              ## indexing for positive coefficients

global v_N_negative_coefs   = zeros(N/2,1);
global v_N_negative_indices = ones(N/2,1);
       s_n_negative_coefs   = 1;            ## indexing for negative coefficients

for n = 1:(N-1) 									# zeroth eigenvalue is assigned to both group on first iteration, the last one (zero) is omited due to degeneracy
    v_E_discretized_eigenfunction = Y{n};
    v_E_weighted_eigenfunction 	  = Y{n}.*t'; 		                                # uses the weighting function of t for orthogonality
   
    integral = trapz(t, v_E_weighted_eigenfunction');                                   # ---> change this to quad - this is only 2nd order accurate and the polynomials are smooth.
        norm = trapz(t, (v_E_weighted_eigenfunction.*v_E_discretized_eigenfunction)');  # compute the normalisation constant

    integral = integral/norm; 								# update the integral value with normalized solution
    
    s_eigenvalue = v_Nr_eigenvalues(n);  
    
    if s_eigenvalue >= 0
	v_N_positive_coefs(s_n_positive_coefs)   =  maxwellian_step*integral;           # see notes for explanation of - sign
	v_N_positive_indices(s_n_positive_coefs) = n;
	s_n_positive_coefs++;
    endif

    if s_eigenvalue <= 0
    	v_N_negative_coefs(s_n_negative_coefs)   = -maxwellian_step*integral;
    	v_N_negative_indices(s_n_negative_coefs) = n;
    	s_n_negative_coefs++;
    endif 
endfor



#####################################################################
## RECONSTRUCT & PLOT THE SOLUTIONS ON INTERVAL AROUND 0
#####################################################################

xneg = linspace(-5, 0, F); # positive eigenvalues part of the solution
xpos = linspace(0, 5, F);  # negative eigenvalues part of the solution

## 
## Returns a row vector of eigenfunctions Phi_n = XU_n evaluated at s_x and s_u.
## s_u has to be integer as Y(n) is a discretized function on linspace and s_u 
## its index
## 
function v_solution = v_eigensolutions(s_x, s_u, v_N_selective_indices, v_eigenvalues, c_v_eigenvectors, s_velocity_param)
	global N;	
	v_solution = zeros(N/2, 1);
	
	for n = 1:(N/2)
	    s_index         = v_N_selective_indices(n);
	    s_eigenvalue    = v_eigenvalues(s_index);
	    v_eigenfunction = c_v_eigenvectors{s_index};
	    v_solution(n) = exp(s_x*s_eigenvalue/s_velocity_param)*v_eigenfunction(s_u);
	endfor
endfunction

function s_y = s_neg(s_x, s_u, s_v)                       # returns function value for x < 0
	global v_N_positive_coefs;
	global v_N_positive_indices;
	global v_Nr_eigenvalues;
	global Y;
	s_y =  v_eigensolutions(s_x, s_u, v_N_positive_indices, v_Nr_eigenvalues, Y, s_v)'*v_N_positive_coefs;
endfunction
	
function s_y = s_pos(s_x, s_u, s_v)                       # returns function value for x > 0
	global v_N_negative_coefs;
	global v_N_negative_indices;
	global v_Nr_eigenvalues;
	global Y;
	s_y = v_eigensolutions(s_x, s_u, v_N_negative_indices, v_Nr_eigenvalues, Y, s_v)'*v_N_negative_coefs; 
endfunction


bool = true;
while(bool)
    for j = 1:F
	    v_neg(j) =	s_neg(xneg(j), s_u, v_1);
	    v_pos(j) =  maxwellian_step + s_pos(xpos(j), s_u, v_2); 
    endfor

    figure(2);
    plot(xneg, v_neg, "", xpos, v_pos, ""); 

    inpt = input("Replot? (Y/N) ", "s");

    if inpt == "N"  
    	break;
    endif

    s_u = input(strcat("New value for s_u? 1:", num2str(E), "\t"));
endwhile


